//
// Checkout.java: implementation of simple checkout module for a cashier system
// Author: Ben Giese
// Assignment: SE3800 - Exercise 6: Cucumber Testing
//

package implementation;

public class Checkout {

  //Requirement: Maximum number of items is 2.
  private int[] prices = new int[2];
  private String[] item_names = new String[2];
  private int[] quantities = new int[2];

  // Adds an item to the cart, and its store price.
   public void setPrice(String item_name, int price) {
     int numItems = numOfItems();
     if(numItems == 2) { //Can't add any more items
       System.out.println("Shopping Cart Full...");
     } else if(numItems == 1) { //If there is 1 item already. Enter second item.
       this.item_names[1] = item_name;
       this.prices[1] = price;
     } else { //No items, enter first item. 
       this.item_names[0] = item_name;
       this.prices[0] = price;
     }
   }

   // Checks if an item is in the cart, if so it updates with its desired quantity.
   public void add(String item_name, int count) {
     if(item_name.equals(this.item_names[0])) {
       this.quantities[0] = count;
     } else if(item_name.equals(this.item_names[1])) {
       this.quantities[1] = count;
     } else {
       System.out.println("No item with this name in your cart...");
     }

   }

   // Returns the shopping cart total
   // Computes the sum of all items, their prices, and their quantities
   public int total() {
      int sum = 0;
      sum = sum + (this.prices[0] * this.quantities[0]);
      sum = sum + (this.prices[1] * this.quantities[1]);
      return sum;
   }

   // returns the number of items in the cart.
   private int numOfItems() {
    int num = -1;
    if(this.prices[1] != 0) {
      num = 2;
    } else if(this.prices[0] != 0 && this.prices[1] == 0) {
      num = 1;
    } else if(this.prices[0] == 0) {
      num = 0;
    } else {
      num = 0;
    }
    return num;
  }
}
