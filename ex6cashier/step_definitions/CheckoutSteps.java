//
// CheckoutSteps.java: step file implementing tests in checkout.features
// Author: Ben Giese
// Assignment: SE3800 - Exercise 6: Cucumber Testing
//

package step_definitions;

import implementation.Checkout;
import cucumber.api.java8.En;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import static org.junit.Assert.*;

public class CheckoutSteps implements En {

  private Checkout checkout = new Checkout();

    public CheckoutSteps() {
      Given("^the price of a (\\w+) is (\\d+)c$", (String item_name, Integer priceInCents) -> {
      checkout.setPrice(item_name,priceInCents);
    });

    When("^I checkout (\\d+) (\\w+)$", (Integer numOfItem, String item_name) -> {
      checkout.add(item_name,numOfItem);
    });

    Then("^the total price should be (\\d+)c$", (Integer totalPriceExpected) -> {
      int totalPriceComputed = checkout.total();
      assertSame(totalPriceExpected, totalPriceComputed);
    });
  }
}
