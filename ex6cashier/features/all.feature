Feature: checking it out different price, number, and items

  Scenario Outline: Checking out with different items, prices, etc.
    Given the price of a <fruit> is <amount>c
    When I checkout 1 <fruit>
    Then the total price should be <amount>c
    
  Examples:
    | fruit | amount |
    | apple | 109    |
    | kumquat | 99   |
    | corn  | 2      |
