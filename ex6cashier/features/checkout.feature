#
# checkout.feature: Cucumber script for cashier system
#

Feature: checking it out

  Scenario: Checking out a banana
    Given the price of a banana is 42c
    When I checkout 1 banana
    Then the total price should be 42c
