Feature: checking multiple items

  Scenario Outline: Check out multiple items with different prices.
    Given the price of an apple is 12c
    And the price of a grape is 2c
    When I checkout 1 apple and 12 grapes
    Then the total price should be 36c
