Feature: checking it out different price, number, and items

  Scenario Outline: Checking out with different quantities.
    Given the price of a grapes is 2c
    When I checkout <quantity> grapes
    Then the total price should be <total>c

  Examples:
    | quantity | total |
    | 1        | 2     |
    | 0        | 0     |
    | 10       | 20    |
    | 25       | 50    |
